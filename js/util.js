function isValid() {
	
	if(arguments.length === 0) {
		return false;
	}
	
	for(var i = 0; i < arguments.length; ++i) {
		var arg = arguments[i];
		if(arg === undefined || arg === null) {
			return false;
		}
	}
	
	return true;
}
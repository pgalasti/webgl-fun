/** Area game will render to **/
var mainRenderer = new THREE.WebGLRenderer({antialias: true});

/** The last valid time between frames **/
var lastTime = 0;

/** Instance of the Tetris Game **/
var Tetris = new TetrisGame(mainRenderer);

$(document).ready(function() {
	main();	
});

/** Used for high resolution timer **/
window.performance = window.performance || {};
performance.now = (function() {
    return performance.now       ||
        performance.mozNow    ||
        performance.msNow     ||
        performance.oNow      ||
        performance.webkitNow ||
        function() {
            return new Date().getTime(); 
        };
})();

/** The game loop the window will update per frame **/
function gameLoop (elapsedTime) {
		
	if(elapsedTime === undefined) {
		elapsedTime = lastTime = 0;
	}
	
	var deltaTime = elapsedTime-lastTime;
	lastTime = elapsedTime;
	
	Tetris.UpdateGameState();
	Tetris.InterpretState(deltaTime);
	
		
	requestAnimationFrame( gameLoop );
};

/** Start method **/
function main() {

	$('#play-area')[0].appendChild( Tetris.MainRenderer.domElement )
	
	Tetris.SetupGame();
	
	gameLoop();
};

/** Window user input listeners **/
document.addEventListener('keydown', Tetris);
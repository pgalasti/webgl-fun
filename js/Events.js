function GEvent(eventType, eventValue) {
	
	debugger;
	this._id = ++GEvent.prototype.NextID;
	
	this.EventType = eventType;
	this.EventValue = eventValue;
	
};
GEvent.prototype.NextID = 0;

GEvent.prototype.printDebug = function() {
	console.log('ID: ' + this._id + ' EventType: ' + this.EventType + ' Value: ' + this.EventValue);
};

function GEventDispatcher() {
	this.Events = [];
};

GEventDispatcher.prototype.push = function(gEvent) {
	this.Events.push(gEvent);
};

GEventDispatcher.prototype.pop = function() {
	
	if(this.nEvents < 1) {
		return;
	}
	
	var gEvent = this.Events[this.Events.length-1];
	
	this.nEvents = this.Events.pop()-1;
	
	return gEvent;
};
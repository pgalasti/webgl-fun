var GAME_STATE_TITLE 				= 'TITLE';
var GAME_STATE_RUNNING 				= 'RUNNING';
var GAME_STATE_PAUSED 				= 'PAUSED';
var GAME_STATE_GAME_OVER 			= 'GAME OVER';

var GAME_EVENT_NEW_SHAPE 			= 'NEW SHAPE';
var GAME_EVENT_MOVE 				= 'MOVE';
var GAME_EVENT_VALUE_LEFT 			= 'LEFT';
var GAME_EVENT_VALUE_RIGHT 			= 'RIGHT';
var GAME_EVENT_VALUE_DOWN 			= 'DOWN';
var GAME_EVENT_VALUE_UP 			= 'UP';

var SHAPE_POSITION_ACTIVE			= 'ACTIVE';
var SHAPE_POSITION_PENDING			= 'PENDING';

var GAME_EVENT_TICK 		= 250; // ms

function TetrisGame(mainRenderer) {
	
	if(!mainRenderer) {
		alert('No main three.js scene renderer! Application will not run!');
		return;
	}
	
	this.Width = window.innerWidth;
	this.Height = window.innerHeight;
	this.Camera = new THREE.PerspectiveCamera( 75, (this.Width/2)/this.Height, 0.1, 1000 );
	
	this.MainRenderer = mainRenderer;
	this.Scene = new THREE.Scene();
	this.GameState = 'TITLE';
	this.ExistingCubes = [];
	this.CurrentShape;
	this.PendingShape;
	this.GameTick = 0;
	this.EventDispatch = new GEventDispatcher();
	
	this.MAX_BOUND_RIGHT = 9;
	this.MAX_BOUND_LEFT = -9;
	this.CEILING = 15;
	this.FLOOR = -14;
	
	this.MainRenderer.setSize( this.Width/2, this.Height );
	
};

TetrisGame.prototype.AddEvent = function(gEvent) {
	this.EventDispatch.push(gEvent);
};

TetrisGame.prototype.SetupGame = function() {
	
	this.DrawPlayAreaBox();
	this.DrawPlayNextShapeAreaBox();
	
	this.Camera.position.set(0, 0, 20);
	
	this.CurrentShape = this.GetNewShape(SHAPE_POSITION_ACTIVE);
	this.PendingShape = this.GetNewShape(SHAPE_POSITION_PENDING);
	
	this.GameState = GAME_STATE_RUNNING;
};

TetrisGame.prototype.InterpretState = function(deltaTime) {
	
	if(this.GameState !== GAME_STATE_RUNNING) {
		return;
	}
	
	this.GameTick += deltaTime;
	if(this.GameTick > GAME_EVENT_TICK) {
		this.GameTick = 0;
		this.CurrentShape.translate(0, -1, 0);
	}
	
	var positions = this.CurrentShape.getPositions();
	for(var i = 0; i < positions.length; ++i) {
		if(this.IsTouchingTopOfObject(positions[i])) {
			this.AddEvent(new GEvent(GAME_EVENT_NEW_SHAPE));
			break;
		}
	}
};

TetrisGame.prototype.UpdateGameState = function() {
	
	var gEvent;
	while(gEvent = this.EventDispatch.pop()) {
			
		if(gEvent.EventType === GAME_EVENT_MOVE) {
			if(gEvent.EventValue === GAME_EVENT_VALUE_LEFT) {
				if(this.CurrentShape.closestXPosition() !== this.MAX_BOUND_LEFT && this.CanMove(GAME_EVENT_VALUE_LEFT)) {
					this.CurrentShape.translate(-1, 0, 0);
				}
			} else if(gEvent.EventValue === GAME_EVENT_VALUE_RIGHT && this.CanMove(GAME_EVENT_VALUE_RIGHT)) {
				if(this.CurrentShape.farthestXPosition() !== this.MAX_BOUND_RIGHT) {
					this.CurrentShape.translate(1, 0, 0);
				}
			} else if(gEvent.EventValue === GAME_EVENT_VALUE_UP) {
				if(this.CurrentShape.farthestXPosition() !== this.MAX_BOUND_RIGHT && this.CurrentShape.closestXPosition() !== this.MAX_BOUND_LEFT) {
					this.CurrentShape.rotate(GAME_EVENT_VALUE_LEFT);
				}
			} else if(gEvent.EventValue === GAME_EVENT_VALUE_DOWN) {
				if(!this.IsShapeTouchingTopOfObject(this.CurrentShape)) {
					this.CurrentShape.translate(0, -1, 0);
				}
			}
		}
		
		if(gEvent.EventType === GAME_EVENT_NEW_SHAPE) {
			this.ExistingCubes = this.ExistingCubes.concat(this.CurrentShape.cubes);
			this.CurrentShape = this.PendingShape;
			this.CurrentShape.initPosition();
			this.CurrentShape.translate(0, this.CEILING, 0);
			
			this.PendingShape = undefined;
			this.PendingShape = this.GetNewShape(SHAPE_POSITION_PENDING);
		}
		
	}
	
	this.MainRenderer.render( this.Scene, this.Camera );
};

TetrisGame.prototype.GetNewShape = function(positionState) {
	
	var shapeCase = Math.floor(Math.random() * 7);
	
	var shape;
	var x = 0;
	var y = this.CEILING;
	var z = 0;
	
	/** Off setting shapes for pending is ugly but whatever **/
	if(shapeCase === 0) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.25;
			y = this.CEILING-6;
		}
		shape = new TetrominoO(this.Scene, x, y, z);
		
	} else if(shapeCase === 1) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.75;
			y = this.CEILING-6;
		}
		shape = new TetrominoI(this.Scene, x, y, z);
		
	} else if(shapeCase === 2) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+4;
			y = this.CEILING-6.5;
		}
		shape = new TetrominoJ(this.Scene, x, y, z);
		
	} else if(shapeCase === 3) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.5;
			y = this.CEILING-6.25;
		}
		shape = new TetrominoL(this.Scene, x, y, z);
		
	} else if(shapeCase === 4) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.75;
			y = this.CEILING-5.25;
		}
		shape = new TetrominoS(this.Scene, x, y, z);
		
	} else if(shapeCase === 5) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.75;
			y = this.CEILING-5;
		}
		shape = new TetrominoT(this.Scene, x, y, z);
		
	} else if(shapeCase === 6) {
		
		if(positionState === SHAPE_POSITION_PENDING) {
			x = this.MAX_BOUND_RIGHT+3.75;
			y = this.CEILING-5;
		}
		shape = new TetrominoZ(this.Scene, x, y, z);
		
	}

	return shape;
	
};

TetrisGame.prototype.IsTouchingTopOfObject = function(position) {
	
	// Check current laid cubes
	for(var i = 0; i < this.ExistingCubes.length; ++i) {
		var existingCube = this.ExistingCubes[i];
		if(existingCube.position.x === position.x && existingCube.position.y === position.y-1) {
			return true;
		}
	}
	
	// Check Floor
	if(position.y <= this.FLOOR) {
		return true;
	}
	
};

TetrisGame.prototype.IsShapeTouchingTopOfObject = function(shape) {
	
	for(var i = 0; i < shape.cubes.length; ++i) {
		var position = shape.cubes[i];
		if(this.IsTouchingTopOfObject(position)) {
			return true;
		}
	}
	
	return false;
};

TetrisGame.prototype.CanMove = function(direction) {
	
	var offset;
	if(direction === 'LEFT') {
		offset = 1;
	} else {
		offset = -1;
	}
	
	var currentCubes = this.CurrentShape.cubes;
	
	for(var i = 0; i < this.ExistingCubes.length; ++i) {
		var existingCube = this.ExistingCubes[i];
		for(var j = 0; j < currentCubes.length; ++j) {
			var currentCube = currentCubes[j];
			if(existingCube.position.x + offset === currentCube.position.x 
				&& existingCube.position.y === currentCube.position.y) {
				return false;
			}	
		}
		
	}
	return true;
};

TetrisGame.prototype.DrawPlayAreaBox = function() {
	
	var material = new THREE.MeshBasicMaterial( { color: 0x0000ff } );
	var geometry = new THREE.Geometry();
	
	var boxLeftSide 	= this.MAX_BOUND_LEFT-1;
	var boxRightSide 	= this.MAX_BOUND_RIGHT+1;
	var boxBottom 		= this.FLOOR-1;
	var boxTopBounds 	= this.CEILING+1;
	
	geometry.vertices.push(new THREE.Vector3( boxLeftSide, 	boxTopBounds, 	0) );
	geometry.vertices.push(new THREE.Vector3( boxLeftSide, 	boxBottom, 		0) );
	geometry.vertices.push(new THREE.Vector3( boxRightSide, boxBottom, 		0) );
	geometry.vertices.push(new THREE.Vector3( boxRightSide, boxTopBounds, 	0) );
	
	var line = new THREE.Line( geometry, material );
	
	this.Scene.add(line);
};

TetrisGame.prototype.DrawPlayNextShapeAreaBox = function() {
	
	var material = new THREE.MeshBasicMaterial( { color: 0x0000ff } );
	var geometry = new THREE.Geometry();
	
	var boxLeftSide = 10.5;
	var boxRightSide = 15;
	var boxBottom = 7;
	var boxTop = 12;
	
	geometry.vertices.push(new THREE.Vector3( boxLeftSide, 	boxTop, 	0) );
	geometry.vertices.push(new THREE.Vector3( boxLeftSide, 	boxBottom, 	0) );
	geometry.vertices.push(new THREE.Vector3( boxRightSide, boxBottom, 	0) );
	geometry.vertices.push(new THREE.Vector3( boxRightSide, boxTop, 	0) );
	geometry.vertices.push(new THREE.Vector3( boxLeftSide, 	boxTop, 	0) );
	
	var line = new THREE.Line( geometry, material );
	
	this.Scene.add(line);
};

var KEY_CODE_LEFT 	= 37;
var KEY_CODE_UP 	= 38;
var KEY_CODE_RIGHT 	= 39;
var KEY_CODE_DOWN 	= 40;

//https://medium.com/@photokandy/til-you-can-pass-an-object-instead-of-a-function-to-addeventlistener-7838a3c4ec62
TetrisGame.prototype.handleEvent = function(evt) {

	var gEvent = new GEvent();

	if(evt.type !== 'keydown') {
		return;
	}
	
	if(evt.keyCode === KEY_CODE_LEFT) {
		gEvent.EventType = GAME_EVENT_MOVE;
		gEvent.EventValue = GAME_EVENT_VALUE_LEFT;
	}
	else if(evt.keyCode === KEY_CODE_UP) {
		gEvent.EventType = GAME_EVENT_MOVE;
		gEvent.EventValue = GAME_EVENT_VALUE_UP;
	}
	else if(evt.keyCode === KEY_CODE_RIGHT) {
		gEvent.EventType = GAME_EVENT_MOVE;
		gEvent.EventValue = GAME_EVENT_VALUE_RIGHT;
	}
	else if(evt.keyCode === KEY_CODE_DOWN) {
		gEvent.EventType = GAME_EVENT_MOVE;
		gEvent.EventValue = GAME_EVENT_VALUE_DOWN;
	}
	
	this.AddEvent(gEvent);
};
var lineMaterial = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 1 } );

function Shape(scene, x, y, z) {
	
	this.cubes = [];
};

Shape.prototype.createCubes = function(material) {

	var cubes = [];
	
	cubes.push(new THREE.Mesh(new THREE.BoxGeometry(), material));
	cubes.push(new THREE.Mesh(new THREE.BoxGeometry(), material));
	cubes.push(new THREE.Mesh(new THREE.BoxGeometry(), material));
	cubes.push(new THREE.Mesh(new THREE.BoxGeometry(), material));
	
	for(var i = 0; i < cubes.length; ++i) {
		var cube = cubes[i];
		
		var edgeGeometry = new THREE.EdgesGeometry( cube.geometry );
		var wireframe = new THREE.LineSegments( edgeGeometry, lineMaterial );
		wireframe.renderOrder = 1;
		cube.add(wireframe);
	}
	
	return cubes;
};

Shape.prototype.translate = function(x, y, z) {
		
	for(var i = 0; i < this.cubes.length; ++i) {
		var cube = this.cubes[i];
		var wireFrame = cube.children[0];
		
		cube.position.x += x;
		cube.position.y += y;
		cube.position.z += z;
	}
};

Shape.prototype.getPositions = function() {
	return [this.cubes[0].position, this.cubes[1].position, this.cubes[2].position, this.cubes[3].position];
};

Shape.prototype.farthestXPosition = function() {
	return Math.max(
		this.cubes[0].position.x, 
		this.cubes[1].position.x, 
		this.cubes[2].position.x, 
		this.cubes[3].position.x
	);
};

Shape.prototype.closestXPosition = function() {
	return Math.min(
		this.cubes[0].position.x, 
		this.cubes[1].position.x, 
		this.cubes[2].position.x, 
		this.cubes[3].position.x
	);
};

/**
*
*	'I' shaped tetromino:
*
*		[ ]
*		[ ]
*		[ ]
*		[ ]
*
**/
function TetrominoI(scene, x, y, z) {
		
	this.cubes = this.createCubes(TetrominoI.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		this.cubes[0].position.y = 2
		this.cubes[1].position.y = 1
		this.cubes[2].position.y = 0
		this.cubes[3].position.y = -1
	};
	
	this.rotate = function(direction) {
			
		var isVertical = this.cubes[0].position.y > this.cubes[3].position.y;
		
		// Before:
		//   [0]
        //   [1]
        //   [2]
		//   [3]
		if(isVertical) {
			this.cubes[0].position.x -= 2;
			this.cubes[0].position.y -= 2;
			
			this.cubes[1].position.x -= 1;
			this.cubes[1].position.y -= 1;
			
			this.cubes[3].position.x += 1;
			this.cubes[3].position.y += 1;
			
			return;
		}
		
		// Before:
		//   
        //   
        //[0][1][2][3]
		//   
		this.cubes[0].position.x += 2;
		this.cubes[0].position.y += 2;
		
		this.cubes[1].position.x += 1;
		this.cubes[1].position.y += 1;
		
		this.cubes[3].position.x -= 1;
		this.cubes[3].position.y -= 1;
	}
	
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoI.prototype = new Shape();
TetrominoI.prototype.material = new THREE.MeshBasicMaterial( { color: 0x00ffff } );

/**
*
*	'O' shaped tetromino:
*
*		[ ][ ]
*		[ ][ ]
*
**/
function TetrominoO(scene, x, y, z) {
	
	this.cubes = this.createCubes(TetrominoO.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		this.cubes[0].position.x = 0;
		this.cubes[1].position.x = 1;
		this.cubes[2].position.y = 1;
		this.cubes[3].position.x = 1;
		this.cubes[3].position.y = 1;
	}
	
	// No need to rotate anything!
	this.rotate = function(direction) {}
	
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoO.prototype = new Shape();
TetrominoO.prototype.material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );

/**
*
*	'J' shaped tetromino:
*
*		[ ]
*		[ ]
*    [ ][ ] 
**/
function TetrominoJ(scene, x, y, z) {
	
	this.state = 1;
	
	this.cubes = this.createCubes(TetrominoJ.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		this.cubes[0].position.y = 2;
		this.cubes[1].position.y = 1;
		this.cubes[2].position.y = 0;
		this.cubes[3].position.x = -1;
		this.cubes[3].position.y = 0;
	}
	
	this.rotate = function() {
		
		// Before:
		//   [0]
        //   [1]
        //[3][2]
		if(this.state === 1) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y -= 2;
			
			this.cubes[1].position.y -= 1;
			
			this.cubes[2].position.x -= 1;
			
			this.cubes[3].position.y += 1;
			
		// Before:
		//   
        //[3]
        //[2][1][0]
		} else if(this.state === 2) {
			
			this.cubes[0].position.x -= 1;
			
			this.cubes[1].position.y += 1;
			
			this.cubes[2].position.x += 1;
			this.cubes[2].position.y += 2;
			
			this.cubes[3].position.x += 2;
			this.cubes[3].position.y += 1;
			
		// Before:
		//   [2][3]
        //   [1]
        //   [0]
		} else if(this.state === 3) {
			
			this.cubes[0].position.x -= 1;
			this.cubes[0].position.y += 1;
			
			//this.cubes[1].position.x += 1;
			
			this.cubes[2].position.y -= 1;
			this.cubes[2].position.x += 1;
			
			this.cubes[3].position.y -= 2;
			
		// Before:
		//   
        //[0][1][2]
        //      [3]	
		} else if(this.state === 4) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y += 1;
			
			this.cubes[2].position.x -= 1;
			this.cubes[2].position.y -= 1;
			this.cubes[3].position.x -= 2;
			
			this.state = 0;
		}
		
		++this.state;
	}
		
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoJ.prototype = new Shape();
TetrominoJ.prototype.material = new THREE.MeshBasicMaterial( { color: 0x0000ff } );

/**
*
*	'L' shaped tetromino:
*
*		[ ]
*		[ ]
*       [ ][ ] 
**/
function TetrominoL(scene, x, y, z) {
	
	this.state = 1;
	
	this.cubes = this.createCubes(TetrominoL.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		this.cubes[0].position.y = 2;
		this.cubes[1].position.y = 1;
		this.cubes[2].position.y = 0;
		this.cubes[3].position.x = 1;
		this.cubes[3].position.y = 0;	
	}
	
	this.rotate = function() {
		
		// Before:
		//   [0]
        //   [1]
        //   [2][3]
		if(this.state === 1) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y -= 1;
			
			this.cubes[2].position.x -= 1;
			this.cubes[2].position.y += 1;
			
			this.cubes[3].position.x -= 2;
			
		// Before:
		// [2][1][0]
        // [3]  
		} else if(this.state === 2) {
			this.cubes[0].position.x -= 1;
			this.cubes[0].position.y -= 2;
			
			this.cubes[1].position.y -= 1;
			
			this.cubes[2].position.x += 1;
			
			this.cubes[3].position.y += 1;
			
			
		// Before:
		// [3][2]
        //    [1]
		//    [0]
		} else if(this.state === 3) {
			this.cubes[0].position.x -= 1;
		
			this.cubes[1].position.y -= 1;
			
			this.cubes[2].position.x += 1;
			this.cubes[2].position.y -= 2;
			
			this.cubes[3].position.x += 2;
			this.cubes[3].position.y -= 1;
			
		// Before:
		//       [3]
		// [0][1][2]
		} else if(this.state === 4) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y += 1;
			
			this.cubes[2].position.x -= 1;
			this.cubes[2].position.y -= 1;
			
			this.cubes[3].position.y -= 2;
			
			this.state = 0;
		}
		
		++this.state;
	}
		
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoL.prototype = new Shape();
TetrominoL.prototype.material = new THREE.MeshBasicMaterial( { color: 0xffa500 } );

/**
*
*	'S' shaped tetromino:
*
*		[ ][ ] 
*	 [ ][ ]
*       
**/
function TetrominoS(scene, x, y, z) {
	
	this.cubes = this.createCubes(TetrominoS.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		//    [2][3] 
		// [0][1]
		this.cubes[0].position.y = -1;
		this.cubes[0].position.x = -1;
		this.cubes[1].position.y = -1;
		this.cubes[2].position.y = 0;
		this.cubes[3].position.x = 1;
	}
	
	this.rotate = function() {
		
		var isVertical = this.cubes[0].position.y > this.cubes[3].position.y;
		
		// Before:
		//    [2][3] 
		// [0][1]
		if(!isVertical) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y += 1;
			
			this.cubes[2].position.x += 1;
			this.cubes[2].position.y -= 1;
			
			this.cubes[3].position.y -= 2;
			
			return;
		}
		
		// Before:
		//    [0] 
		//    [1][2]
		//       [3]
		this.cubes[0].position.x -= 1;
		this.cubes[0].position.y -= 1;
			
		this.cubes[2].position.x -= 1;
		this.cubes[2].position.y += 1;
			
		this.cubes[3].position.y += 2;
			
	}
		
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoS.prototype = new Shape();
TetrominoS.prototype.material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );

/**
*
*	'T' shaped tetromino:
*
*	 [ ][ ][ ] 
*	    [ ]
*       
**/
function TetrominoT(scene, x, y, z) {
	
	this.cubes = this.createCubes(TetrominoT.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		// [0][1][2] 
		//    [3]
		this.cubes[0].position.x = -1;
		this.cubes[2].position.x = 1;
		this.cubes[3].position.y = -1;
	}
	
	this.state = 1;
	
	this.rotate = function() {
	
		
		// Before:
		// [0][1][2] 
		//    [3]
		if(this.state === 1) {
			
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y += 1;
			
			this.cubes[2].position.x -= 1;
			this.cubes[2].position.y -= 1;
			
			this.cubes[3].position.x -= 1;
			this.cubes[3].position.y += 1;
			
		// Before:
		//    [0]
		// [3][1]
		//    [2]
		} else if(this.state === 2) {
			this.cubes[0].position.x += 1;
			this.cubes[0].position.y -= 1;
			
			this.cubes[2].position.x -= 1;
			this.cubes[2].position.y += 1;
			
			this.cubes[3].position.x += 1;
			this.cubes[3].position.y += 1;
			
		// Before:
		//    [3]
		// [2][1][0]
		//    
		} else if(this.state === 3) {
		
			this.cubes[0].position.x -= 1;
			this.cubes[0].position.y -= 1;
			
			this.cubes[2].position.x += 1;
			this.cubes[2].position.y += 1;
			
			this.cubes[3].position.x += 1;
			this.cubes[3].position.y -= 1;
			
		// Before:
		//    [2]
		//    [1][3]
		//    [0]		
		} else if(this.state === 4) {
			this.cubes[0].position.x -= 1;
			this.cubes[0].position.y += 1;
			
			this.cubes[2].position.x += 1;
			this.cubes[2].position.y -= 1;
			
			this.cubes[3].position.x -= 1;
			this.cubes[3].position.y -= 1;
			
			this.state = 0;
		}
		++this.state;
	}
	
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoT.prototype = new Shape();
TetrominoT.prototype.material = new THREE.MeshBasicMaterial( { color: 0x551a8b } );

/**
*
*	'Z' shaped tetromino:
*
*	 [ ][ ] 
*	    [ ][ ]
*       
**/
function TetrominoZ(scene, x, y, z) {
	
	this.cubes = this.createCubes(TetrominoZ.prototype.material);
	
	for(var i = 0; i < this.cubes.length; ++i) {
		scene.add(this.cubes[i]);
	}
	this.initPosition = function() {
		this.cubes[0].position.x = this.cubes[0].position.y = this.cubes[0].position.z = 0;
		this.cubes[1].position.x = this.cubes[1].position.y = this.cubes[1].position.z = 0;
		this.cubes[2].position.x = this.cubes[2].position.y = this.cubes[2].position.z = 0;
		this.cubes[3].position.x = this.cubes[3].position.y = this.cubes[3].position.z = 0;
		
		// [0][1]
		//    [2][3]
		this.cubes[0].position.x = -1;
		this.cubes[2].position.y = -1;
		this.cubes[3].position.y = -1;
		this.cubes[3].position.x = 1;	
	}
	
	this.rotate = function() {
		
		var isVertical = this.cubes[0].position.y < this.cubes[3].position.y;
		
		// Before:
		//
		// [0][1]
		//    [2][3] 
		if(!isVertical) {
			
			this.cubes[0].position.y -= 1;
			
			this.cubes[1].position.x -= 1;
			
			this.cubes[2].position.y += 1;
			
			this.cubes[3].position.x -= 1;
			this.cubes[3].position.y += 2;
			
			return;
		}
		
		// Before:
		//    [3] 
		// [1][2]
		// [0]
		this.cubes[0].position.y += 1;
			
		this.cubes[1].position.x += 1;
			
		this.cubes[2].position.y -= 1;
			
		this.cubes[3].position.x += 1;
		this.cubes[3].position.y -= 2;
			
	}
	
	this.initPosition();
	if(isValid(x, y, z)) {
		this.translate(x, y, z);
	}
	
};

TetrominoZ.prototype = new Shape();
TetrominoZ.prototype.material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );